var Models = require('../Models');
var async = require('async');
var Utils = require('../Utils');
var Configs = require('../Configs');
var jwt = require('jsonwebtoken');
module.exports = {
	registerUser : function (params,callback) {
		async.waterfall(
            [
                function(cb) { // check if email is already registred with us
                	if(params.email_id)
                	{
	                    Utils.commonFunctions.check_email_exist(params.email_id, function(err, res) {
	                        if (err) {
	                            cb(Utils.responses.systemError)
	                        } else {
	                            if (res.length == 0) { //new account cretaed
                                    if(params.fb_id || params.twitter_id)
                                    {
                                        var obj={};
                                        if(params.auth_type==1)
                                            obj["fb_id"]=params.fb_id
                                        else if(params.auth_type==2)
                                            obj["twitter_id"]=params.twitter_id;
                                        Models.users.User.findOneAndUpdate(obj,{email:params.email_id},function(err, res) {
                                            if (err) {
                                                Utils.logger.errorLogger('Error in token save process', err);
                                                cb(err, null);
                                            } else {
                                                if(res && Object.keys(res).length>0)
                                                {
                                                    var token = jwt.sign({
                                                            id: res._id,
                                                            email: res.email,
                                                            role: res.role
                                                        },
                                                        Configs.config.data.jwtkey, { algorithm: Configs.config.data.jwtAlgo, expiresIn: '2 days' }
                                                    );
                                                    if (res.is_suspended == true) {
                                                        cb(Utils.responses.suspended)
                                                    } else if (res.is_deleted == true) {
                                                        cb(Utils.responses.deleted)
                                                    } else if (res.is_confirmed == false) {
                                                        cb(Utils.responses.notconfirmed)
                                                    }else{
                                                        Models.users.User.findOneAndUpdate({ _id: res._id },{login_token:token}, function(err, res) {
                                                            if (err) {
                                                                Utils.logger.errorLogger('Error in token save process', err);
                                                                cb(err, null);
                                                            } else {
                                                                Utils.logger.successLogger('Token', res);
                                                                cb({statusCode: 200, status: "success", description: "Logged in successfully",data:{ auth_token: token ,user_id:res._id,fullname:res.fullname}});
                                                                // Utils.responses.loginSuccessfull["data"]={ auth_token: token ,user_id:res._id,fullname:res.fullname};
                                                                // cb(Utils.responses.loginSuccessfull);
                                                            }
                                                        });
                                                    }
                                                }else
                                                cb(null, params);
                                            }
                                        });
                                    }
	                            	else
	                                	cb(null, params);
	                            } else { // email exist but signup with fb or twitter then login and updated ids
	                            	var obj={};
                                    if(params.auth_type==0)
                                        cb(Utils.responses.emailAlreadyExists);
                                    else if(params.auth_type!=0)
                                    {
                                        if(params.auth_type==1)
                                            obj["fb_id"]=params.fb_id
                                        else if(params.auth_type==2)
                                            obj["twitter_id"]=params.twitter_id;
    	                            	if (res[0].is_suspended == true) {
    				                        cb(Utils.responses.suspended)
    				                    } else if (res[0].is_deleted == true) {
    				                        cb(Utils.responses.deleted)
    				                    } else if (res[0].is_confirmed == false) {
                                            cb(Utils.responses.notconfirmed)
                                        } else{
    				                        var token = jwt.sign({
    					                            id: res[0]._id,
    					                            email: res[0].email,
    					                            role: res[0].role
    					                        },
    					                        Configs.config.data.jwtkey, { algorithm: Configs.config.data.jwtAlgo, expiresIn: '2 days' }
    					                    );
    					                    obj["login_token"]=token;
    					                    Models.users.User.findOneAndUpdate({ _id: res[0]._id }, obj, function(err, res) {
    					                        if (err) {
    					                            Utils.logger.errorLogger('Error in token save process', err);
    					                            cb(err, null);
    					                        } else {
    					                            Utils.logger.successLogger('Token', res);
                                                    cb({statusCode: 200, status: "success", description: "Logged in successfully",data:{ auth_token: token ,user_id:res._id,fullname:res.fullname}});
                                                    // Utils.responses.loginSuccessfull["data"]={ auth_token: token ,user_id:data._id};
                                                    // cb(Utils.responses.loginSuccessfull);
    					                        }
                         					});
    				                    }
                                    }
	                            }
	                        }
	                    });
                	}
                	else
                	{
                		var fb=0,twt=0;
                		if(params.fb_id)
                			fb=params.fb_id;
                		if(params.twitter_id)
                			twt=params.twitter_id
                		Models.users.User.find({$or:[{"fb_id":fb},{"twitter_id":twt}]},function(err,res){
                			if(err)
                				cb(Utils.responses.systemError)
                			else
                			{
                                console.log(res);
                				if(res.length==0)
                					cb(null,params);
                				else
                				{ 	//login if fb_id or twitter_id exist then login

                					var obj={};
	                            	if(params.auth_type==1)
	                            		obj["fb_id"]=params.fb_id
	                            	else if(params.auth_type==2)
	                            		obj["twitter_id"]=params.twitter_id;
	                            	else
	                            		cb(Utils.responses.emailAlreadyExists);
	                            	if (res[0].is_confirmed == false) {
				                        cb(Utils.responses.notconfirmed)
				                    } else if (res[0].is_suspended == true) {
				                        cb(Utils.responses.suspended)
				                    } else if (res[0].is_deleted == true) {
				                        cb(Utils.responses.deleted)
				                    } else {
				                        var token = jwt.sign({
					                            id: res[0]._id,
					                            email: res[0].email,
					                            role: res[0].role
					                        },
					                        Configs.config.data.jwtkey, { algorithm: Configs.config.data.jwtAlgo, expiresIn: '2 days' }
					                    );
					                    obj["login_token"]=token;
                                        Models.users.User.findOneAndUpdate({ _id: res[0]._id }, obj, function(err, data) {
					                        if (err) {
					                            Utils.logger.errorLogger('Error in token save process', err);
					                            cb(err, null);
					                        } else {
                                                Utils.logger.successLogger('Token', data);
                                                Utils.responses.loginSuccessfull["data"]={ auth_token: token ,user_id:res._id};
                                                cb(Utils.responses.loginSuccessfull);
					                        }
                    					});
				                    }
                				}
                			}
                		});
                		
                	}
                },
                function(data, cb) { // hashing password
                    if (data.password) {
                        var password_hash = Utils.commonFunctions.encryptpassword(data.password);
                        console.log("password generated is", password_hash)
                    } else {
                    	if(data.email_id)
                    		var tp=data.email_id;
                    	else
                    		var tp=data.fb_id ? data.fb_id : data.twitter_id;
                        var password_hash = Utils.commonFunctions.encryptpassword(tp);
                    }

                    var token = ''; //generate token
                    if(data.email_id)
                    		var tp=data.email_id;
                    	else
                    		var tp=data.fb_id ? data.fb_id : data.twitter_id;
                    Utils.commonFunctions.generate_token(tp, function(err, res) {
                        if (err) {
                            Utils.logger.errorLogger('Error in generating token', err);
                            cb(Utils.responses.systemError);
                        } else {
                            var obj={};
                            var tok=Utils.commonFunctions.makeid();
                            token = res;
                            if (data.auth == 0) {
                            	obj = {
	                                auth_type: data.auth_type,
	                                role: data.role,
	                                email: data.email_id,
	                                password: password_hash,
	                                login_token: token,
                                    token:tok,
	                                modified_at: Date.now()
                            	};
                            }
                            else
                            {
                            	obj = {
	                                auth_type: data.auth_type,
	                                role: data.role,
	                                fullname: data.fname,
	                                email: data.email_id,
	                                gender:data.gender,
	                                contactNumber:data.contactNumber,
	                                dob:data.dob,
	                                gcm_id:data.gcm_id,
	                                fb_id:data.fb_id,
	                                twitter_id:data.twitter_id,
	                                device_token:data.device_token,
	                                password: password_hash,
	                                login_token: token,
                                    token:tok,
	                                modified_at: Date.now()
                            	};
                        	}
                            cb(null, obj)
                        }
                    });
                },
                function(data, cb) {
                    var object = data; // Save the record to the database                        
                    Models.users.User(object).save(function(err, res) {
                        if (err) {
                            console.log("error in saving datasss"+err);
                            Utils.logger.errorLogger('Error in saving registration data in database', err);
                            cb(Utils.responses.systemError);
                        } else {
                            console.log("saving data in db"+res)
                            if(data.email)
                            {
	                            var email_data = {
	                                email: data.email,
	                                login_token: data.login_token,
	                                role: data.role,
	                                user_id:res._id,
                                    token:data.token
	                            }
	                            cb(null, email_data)
                        	}
                        	else
                        	{
                        		Utils.responses.emailEmpty["data"]={user_id:res._id,token:res.token,verifytoken:data.token};
                        		cb(Utils.responses.emailEmpty);
                        	}
                        }

                    });
                },
                function(data, cb) { // send the email to user with verfication link
                        var email_data = { 
                            to: data.email,
                            from: '"Traffic Jam Admin " <' + Configs.config.data.noReplyEmail + '>',
                            subject: 'Traffic Jam - Account Confirmation',
                            html: '<b>Hi,<br> you have registered new account with Traffic Jam . Please <a href="'+Configs.app.url+'/v1/Users/verifyAccount/'+escape(data.token)+'" >click</a> to verify your account.</b>'
                        }
                        console.log(email_data);
                    Utils.commonFunctions.send_email(email_data, function(err, res) {
                        if (err) {
                        	console.log("error in mail send");
                            Utils.logger.errorLogger('Error in register email sending', err);
                        }
                        Utils.logger.successLogger('Response of registration mail', res);
                        Utils.responses.registerationSuccessfull["data"]={user_id:data.user_id,auth_token:data.login_token,verifytoken:data.token};
                        cb(null, Utils.responses.registerationSuccessfull)
                    });
                }
            ],
            function(err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
            });
	}
};
