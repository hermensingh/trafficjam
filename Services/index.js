/*
 * @file:index
 * @description:access all service files
 * @date:21/09/2016
 * @author:Harpreet/Hermenpreet
 * */

module.exports={
	user: require('./users'),
	signUp: require('./signUp'),
	jammer:require('./jammer')
};

