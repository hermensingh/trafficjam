var Models = require('../Models');
var Utils = require('../Utils');
var Configs = require('../Configs');
var async = require('async');
var jwt = require('jsonwebtoken');
module.exports = {
    jammerInsert: function(params, callback) {   //done
        async.waterfall([
            function(cb){
                params["k"]="audio";
                Utils.commonFunctions.uploadProfilePic(params,function(err,res){
                    if (err) {
                            cb(Utils.responses.systemError)
                        } 
                        else
                        {
                            params.url=res.imagename;
                            cb(null, params);
                        }
                });
            },
            function(params,cb) { 
                var object={
                    user_id:params.user_id,
                    audiourl:params.url,
                    filetype:params.type,
                    duration:params.duration
                };
                Models.jamfiles.Jamfiles(object).save(function(err, res) {
                    if (err) {
                        console.log("error in saving datasss"+err);
                        Utils.logger.errorLogger('Error in saving Jam audio in database', err);
                        cb(Utils.responses.systemError);
                    } else {
                        Utils.responses.jamInserted["data"]={audiourl:params.url};
                            cb(null,Utils.responses.jamInserted);
                    }
                });
                }                
            ],
            function(err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
            });
    }
};

