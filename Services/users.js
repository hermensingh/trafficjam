var Models = require('../Models');
var Utils = require('../Utils');
var Configs = require('../Configs');
var async = require('async');
var jwt = require('jsonwebtoken');
module.exports = {
	login: function(params, callback) {   //done
        async.waterfall([
                function(cb) { // check if email exist with us
                    Utils.commonFunctions.check_email_exist(params.email, function(err, res) {
                        if (err) {
                            cb(Utils.responses.systemError)
                        } else {
                            if (res.length == 0) {
                                cb(Utils.responses.emailNotExists); //done
                            } else {
                                cb(null, params);
                            }
                        }
                    });
                },
                function(data, cb) { // check if password is correct for the email provided
                    var password_hash = Utils.commonFunctions.encryptpassword(data.password);
                    var obj = {
                        email: data.email,
                        password: password_hash
                    }
                    Utils.commonFunctions.check_email_password(obj, function(err, res) {
                        if (err) {
                            cb(Utils.responses.systemError)
                        } else {                     
                            if (res == null) {
                                cb(Utils.responses.invalidpassword) //done
                            } else {
                                res.device_token=data.device_token;
                                res.gcm_id=data.gcm_id;
                                cb(null, res)
                            }
                        }
                    });
                },
                function(data, cb) { // check if account is confirmed                                          
                    if (data.is_confirmed == false) {
                        cb(Utils.responses.notconfirmed)
                    } else if (data.is_suspended == true) {
                        cb(Utils.responses.suspended)
                    } else if (data.is_deleted == true) {
                        cb(Utils.responses.deleted)
                    } else {
                        cb(null, data)
                    }
                },
                function(data, cb) {
                    var token = jwt.sign({
                            id: data._id,
                            email: data.email,
                            role: data.role
                        },
                        Configs.config.data.jwtkey, { algorithm: Configs.config.data.jwtAlgo}
                    );
                    Models.users.User.findOneAndUpdate({ _id: data._id }, {device_token:data.device_token,gcm_id:data.gcm_id,login_token: token }, function(err, res) {
                        if (err) {
                            Utils.logger.errorLogger('Error in token save process', err);
                            cb(err, null);
                        } else {
                            Utils.logger.successLogger('Token', res);
                            Utils.responses.loginSuccessfull["data"]={ auth_token: token ,user_id:data._id};
                            cb(null,Utils.responses.loginSuccessfull);
                        }
                    });

                }

            ],
            function(err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
            });

    },
    forgotPassword: function(params, callback) { // Reset password request
        async.waterfall([
                function(cb) { // check if email exist with us
                    Utils.commonFunctions.check_email_exist_and_update(params.email, function(err, res) {
                        if (err) {
                            cb(err)
                        } else {
                            if (res.length == 0) {
                                cb(Utils.responses.emailNotExists);
                            } else {
                                params.token=res[0].tk;
                                cb(null, params);
                            }
                        }
                    });
                },
                function(data, cb) {
                    var url=Configs.app.url.substr(0,(Configs.app.url.lastIndexOf(":")));
                    var email_data = { // set email variables
                        to: data.email,
                        from: Configs.config.data.adminEmail,
                        subject: 'Traffic Jam - Forget Password Request',
                        html: '<b>Hi,<br> your requested for reset password. please <a href="'+url+'/admin/index.html?token='+data.token+'" >click here</a>'
                    };
                    Utils.commonFunctions.send_email(email_data, function(err, res) { // send password reset email to the admin
                        if (err) {
                            Utils.logger.errorLogger('Error in register email sending', err);
                        }
                        Utils.logger.successLogger('Response of registration mail', res);
                        cb(null, Utils.responses.resetPasswordSuccessfull)
                    });

                }
            ],
            function(err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
            });
    },
    logOutUser:function(params,callback){
        async.waterfall([
            function(cb){
               Utils.commonFunctions.check_id_exist(params.id,function(err,res){
                    if(err)
                        cb(Utils.responses.systemError);
                    else{
                        if(res.length==0){
                          cb(Utils.responses.idNotExist);
                         } 
                         else
                            cb(null, params);
                    }
               });
            },
            function(data,cb){
                    Models.users.User.findOneAndUpdate({_id:data.id}, { login_token: "0" },function(err,res){
                       if(err)
                        cb(Utils.responses.systemError);
                      else
                        cb(null,Utils.responses.logoutSuccessfull);
                    });
            }
            ],function(err,result){
             if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
        });
    },
     changepassword: function(params, callback) // Reset password using old password 
     { 
        async.waterfall(
            [
                function(cb) { // Is old password is correct?
                    var oldpassword_hash = Utils.commonFunctions.encryptpassword(params.oldpassword);
                    var obj = {
                        userid: params.userid,
                        oldpassword: oldpassword_hash
                    }
                    Utils.commonFunctions.check_old_password(obj, function(err, res) {
                        if (err) {
                            cb(Utils.responses.systemError)
                        } else {
                            if (res.length == 0) {
                                cb(Utils.responses.invalidpassword)
                            } else {
                                cb(null, params)
                            }
                        }
                    });
                },
                function(data, cb) { // update password.
                    var newpassword_hash = Utils.commonFunctions.encryptpassword(data.newpassword);
                    Models.users.User.findOneAndUpdate({ _id: data.userid }, { password: newpassword_hash }, function(err, res) {
                        if (err) {
                            Utils.logger.errorLogger('Error in reset password process', err);
                            cb(err, null);
                        } else {
                            Utils.logger.successLogger('Password updated.', res);
                            Utils.responses.updatepassword["data"]={auth_token:res.login_token,user_id:res._id};
                            cb(null, Utils.responses.updatepassword)
                        }

                    });
                }

            ],
            function(err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
            });
    },
    getuserProfile: function(params, callback){
             async.waterfall([
            function(cb){
               Utils.commonFunctions.check_id_exist(params,function(err,res){
                    if(err)
                        cb(Utils.responses.systemError);
                    else{
                        if(res.length==0){
                          cb(Utils.responses.idNotExist);
                         } 
                         else
                            cb(null, params);
                    }
               });
            },
            function(data,cb){
                Models.users.User.findOne({ _id: data },{_id:1,fullname:1,contactNumber:1,imageUrl:1,email:1,gender:1}, function(err, res) {
                    if(err)
                        cb(Utils.responses.systemError);
                      else
                        cb(null,res);
                  });
            }
            ],function(err,result){
             if (err) {
                    callback(err)
                } else {
                    if(result.imageUrl)
                        result.imageUrl=Configs.app.url+""+result.imageUrl;
                    callback(null, result)
                }
        });
    },
    updateprofile: function(params, callback){
        async.waterfall([
            function(cb){
                var obj ={
                    logintoken: params.payload.logintoken,
                    userid: params.params.user_id
                }
                Utils.commonFunctions.check_login_token_and_userid_exist(obj,function(err, res){
                    if(err){
                        cb(Utils.responses.idOrTokenNotExist)
                    } else{
                        cb(null, params)
                    }
                });
            },function(data,cb){
                Models.users.User.find({ username: data.payload.username },function(err, res) {
                        if (err) {
                            Utils.logger.errorLogger('Error in user found', err);
                            cb(err);
                        } else {
                            if(res.length==0)
                                cb(null,data);
                            else
                                cb(Utils.responses.usernameAlreadyExists);
                        }
                    });
            },
            function(data, cb){
                var obj = {
                            fullname:data.payload.fullname,
                            contactNumber: data.payload.mobile,
                            dob: data.payload.dob,
                            gender: data.payload.gender,
                            modified_at:Date.now(),
                            username:data.payload.username
                        }                             
                    Models.users.User.findOneAndUpdate({ _id: data.params.user_id }, obj, function(err, res) {
                        if (err) {
                            Utils.logger.errorLogger('Error in profile updation', err);
                            cb(err);
                        } else {
                            Utils.logger.successLogger('Token', res);
                            Utils.responses.profileUpdated["data"]={"fullname":obj.fullname,dob:obj.dob,user_id:res._id,mobile:obj.contactNumber,gender:obj.gender,login_token:res.login_token,"username":obj.username};
                            cb(null,Utils.responses.profileUpdated);
                        }
                    });
            }

            ],
            function(err,result){
                if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
            });
    },
    verifyActivation: function(params, callback) { // verify account process 
        async.waterfall(
            [
                function(cb) { // check if token exist in the database
                    Utils.commonFunctions.check_tok_exist(params.token, function(err, res) {
                        if (err) {
                            cb(Utils.responses.systemError)
                        } else {
                            if (res.length == 0) {
                                cb(Utils.responses.tokenNotExist)
                            } else {
                                if(res.length>0 && !res[0].email)
                                    cb(Utils.responses.emailIdNotExists)
                                else
                                    cb(null, params)
                            }
                        }
                    });
                },
                function(data, cb) { // if token exist, then update user status.
                    Models.users.User.findOneAndUpdate({ token: data.token }, { is_confirmed: true, token: 0 }, function(err, res) {
                        if (err) {
                            Utils.logger.errorLogger('Error in verification process', err);
                            cb(err, null);
                        } else {
                            Utils.logger.successLogger('Response of verification account(token)', res);
                            cb(null, Utils.responses.isConfirmed);
                        }
                    });
                }
            ],
            function(err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
            });
    },
    resetNewPassword: function(params, callback) {
        async.waterfall([
        function(cb) { // check if token exist in the database
                    Models.users.User.find({reset_token:params.resetpassword_token}, function(err, res) {
                        if (err) {
                            cb(Utils.responses.systemError)
                        } else {
                            if (res.length == 0) {
                                cb(Utils.responses.tokenNotExist)
                            } else {
                                cb(null, params)
                            }
                        }
                    });
                },
        function(resp, cb) {
            var newpassword_hash = Utils.commonFunctions.encryptpassword(params.newpassword);
            Models.users.User.findOneAndUpdate({ reset_token: params.resetpassword_token }, { password: newpassword_hash,reset_token: "0" }, function(err, res) {
            if (err) {
                Utils.logger.errorLogger('Error in reset password process', err);
                cb(err, null);
            } else {
                Utils.logger.successLogger('Password updated.', res);
                cb(null, Utils.responses.updatepassword);
            }
            });
        }
        ], function(err, result) {
        if (err) {
            callback(err)
        } else {
            callback(null, result)
        }
        })
    },
    uploadImg:function(params,callback){
        async.waterfall([
            function(cb){
                Utils.commonFunctions.uploadProfilePic(params,function(err,res){
                    if (err) {
                            cb(Utils.responses.systemError)
                        } 
                        else
                        {
                            params.url=res.imagename;
                            cb(null, params);
                        }
                });
        },function(resp,cb)
        {
            Models.users.User.findOneAndUpdate({ _id: resp.user_id }, {imageUrl:resp.url}, function(err, res) {
            if (err) {
                Utils.logger.errorLogger('Error in reset Image process', err);
                cb(err, null);
            } else {
                Utils.logger.successLogger('Image updated.', res);
                Utils.responses.imageUploadSuccessfull["data"]={"imageUrl":Configs.app.url+""+resp.url,logintoken:res.login_token,user_id:res._id};
                cb(null, Utils.responses.imageUploadSuccessfull);
            }
            });
        }
        ],
        function(err,result){
            if (err) {
            callback(err)
        } else {
            callback(null, result)
        }
        });
    },
    emailUpdateAndVerify:function(params,callback){
        async.waterfall([
            function(cb)
            {
                Utils.commonFunctions.check_email_exist(params.email, function(err, res) {
                if (err) {
                    cb(Utils.responses.systemError)
                } else {
                    Utils.logger.successLogger('check Email.', res);
                    if (res.length == 0)
                        cb(null,params);
                    else
                        cb(Utils.responses.emailAlreadyExists);
                }
                });
            },
            function(data,cb)
            {
                console.log(data);
                Models.users.User.findOneAndUpdate({ _id: data.id,token:data.token }, {email:data.email}, function(err, res) {
                if (err) {
                    Utils.logger.errorLogger('Error in reset update email', err);
                    cb(err, null);
                } else {
                    Utils.logger.successLogger('email updated.', res);
                    if(res)
                        cb(null,data);
                    else
                        cb(Utils.responses.tokenNotExist);
                }
                });
            },
            function(data, cb) { // send the email to user with verfication link
                        var email_data = { 
                            to: data.email,
                            from: '"Traffic Jam Admin " <' + Configs.config.data.noReplyEmail + '>',
                            subject: 'Traffic Jam - Account Confirmation',
                            html: '<b>Hi,<br> you have registered new account with Traffic Jam . Please <a href="'+Configs.app.url+'/v1/Users/verifyAccount/'+escape(data.token)+'" >click</a> to verify your account.</b>'
                        }
                        console.log(email_data);
                    Utils.commonFunctions.send_email(email_data, function(err, res) {
                        if (err) {
                            Utils.logger.errorLogger('Error in register email sending', err);
                        }
                        Utils.logger.successLogger('Response of registration mail', res);
                        Utils.responses.registerationSuccessfull["data"]={user_id:data.user_id,auth_token:data.login_token};
                        cb(null, Utils.responses.registerationSuccessfull)
                    });
                }
        ],
        function(err,result){
            if (err) {
                callback(err)
            } else {
                callback(null, result)
            }
        });
    },
    upadteLocation: function(params, callback) {   
        async.waterfall([
                function(cb) { 
             Models.users.User.findOneAndUpdate({ _id: params.user_id }, {"location.coordinates":[params.latitude,params.longitude],modified_at:Date.now()}, function(err, res) {
                    if (err) {
                        Utils.logger.errorLogger('Error in location update', err);
                        cb(err, null);
                    } else {
                        Utils.logger.successLogger('location updated.', res);
                        Utils.responses.locationSuccessfull["data"]={user_id:res._id,login_token:res.login_token};
                        cb(null, Utils.responses.locationSuccessfull);
                    }
                });
                }
            ],
            function(err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, result)
                }
            });

    }
};

