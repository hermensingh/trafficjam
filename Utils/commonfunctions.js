/*
 * @file: commonfunctions.js-Utils
 * @description: common functions that can be used throughout the code
 * @date: 30-08-2016
 * @author: Harpreet/Hermenpreet
 * */

var Models = require('../Models');
var jwt = require('jsonwebtoken');
var md5 = require('md5');
var Utils = require('./mailer');
var transporter = Utils.transporter;
var bcrypt = require('bcryptjs');
var async = require('async');
var messages = require('./responses');
var jwt = require('jsonwebtoken');
var Configs = require('../Configs');
const saltRounds = 10;
var path = require('path');
var fs=require("fs");

const env = require('../env');
const app = (env == "dev") ? Configs.app.test   :  Configs.app.dev;
module.exports = {
    encryptpassword: function(request) {
        return md5(request);
    },
    generate_token: function(request, callback) {
        bcrypt.hash(request, saltRounds, callback);
    },
    check_email_exist: function(request, callback) {
        Models.users.User.find({ email: request }, function(err, res) {
            callback(err, res);
        });
    },
    check_email_exist_and_update: function(request, callback) {
        var tk=this.makeid();
        Models.users.User.findOneAndUpdate({ email: request},{reset_token:tk} ,function(err, res) {
            if(err)
                callback(messages.systemError);
            else 
            {
                var abc=[];
                if(res)
                {
                    res.tk=tk;
                    abc.push(res);
                }
                callback(err, abc);               
            }
        });
    },
    check_id_exist: function(request, callback) {
            Models.users.User.find({ _id: request }, function(err, res) { // find id im users collection
                callback(err, res);
            });
       },
    check_email_password: function(request, callback) {
        Models.users.User.findOne({ email: request.email, password: request.password }, function(err, res) {
            callback(err, res);
        });
    },
    send_email: function(request, cb) {
        var mailOptions = {
            from: request.from, // sender address 
            to: request.to, // list of receivers 
            subject: request.subject, // Subject line 
            text: '', // plaintext body 
            html: request.html // html body 
        };
        transporter.sendMail(mailOptions, function(error, info) { // send mail with defined transport object 
            cb(error, info)
        });
    },
    check_old_password: function(request, callback) {
        Models.users.User.find({ _id: request.userid, password: request.oldpassword }, function(err, res) {
            callback(err, res);
        });

    },
    require_login: function(request, reply) { // validate the given token 
        if (request.payload == null) {
            var token = request.params.logintoken;
        } else {
            var token = request.payload.logintoken;
        }
        async.waterfall([
            function(cb) {
                console.log("=-=-=-=-"+token);
                jwt.verify(token, Configs.config.data.jwtkey, function(err, decode) { // checking token expiry
                    if (err) {
                        console.log(err);
                        cb(messages.tokenNotExist)
                    } else {

                        cb(null, decode);
                    }
                })
            },
            function(decodedata, cb) {
                Models.users.User.find({ login_token: token, _id: decodedata.id }, function(err, res) { // verifying token and respective userid existence in db
                    if (err) {
                        cb(messages.systemError)
                    } else {
                        if (res.length == 0) {
                            cb(messages.idOrTokenNotExist);
                        } else {
                            console.log("successfully proceed no error...")
                            cb(null, { data: decodedata })
                        }
                    }
                })

            }
        ], function(err, result) {
            if (err) {
                reply(err).takeover();
            } else {
                reply(result);
            }

        })
    },check_login_token_exist: function(request, callback) {
        Models.users.User.find({ login_token: request }, function(err, res) {
            callback(err, res);
        });
    },
    check_token_exist: function(request, callback) {
        Models.users.User.find({ login_token: request }, function(err, res) {
            callback(err, res);
        });
    },check_tok_exist: function(request, callback) {
        Models.users.User.find({ token: request }, function(err, res) {
            callback(err, res);
        });
    },
    check_login_token_and_userid_exist: function(request, callback) {
        Models.users.User.find({ login_token: request.logintoken, _id: request.userid }, function(err, res) {
            callback(err, res);
        });
    },
    uploadProfilePic: function(data, cb) { // upload images
        if(data.k="audio")
        {
            var filename = "audio" + Math.floor(Date.now() / 1000) + "." + data.type;
            var dest = path.join('./Assets/Audio/', filename);
        }
        else
        {
            var filename = "profile" + Math.floor(Date.now() / 1000) + "." + data.type;
            var dest = path.join('./Assets/Images/', filename);
        }
        fs.writeFile(dest, data.buffer, function(err) { // write the stream to file at a dest(path where to be stored)
            if (err) {
                console.log(err)
                cb(messages.fileWriteError)
            } else {
                if(data.k="audio")
                    obj.imagename = "/Assets/Audio/"+filename;
                else
                    obj.imagename = "/Assets/Images/"+filename;
                cb(null, obj)
            }
        });
    },
    makeid:function()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text+""+Math.floor(Date.now() / 1000);
    },
    failActionFunction: function(request, reply, source, error) {
        var Msg = '';
        if (error.output.payload.message.indexOf("[") > -1) {
            Msg = error.output.payload.message.substr(error.output.payload.message.indexOf("["));
        } else {
            Msg = error.output.payload.message;
        }
        error.output.payload['status']=400;
        delete error.output.payload.statusCode;
        delete error.output.payload.error;
        delete error.output.payload.message;
        delete error.output.payload.validation;
        Msg = Msg.replace(/"/g, '');
        Msg = Msg.replace('[', '');
        Msg = Msg.replace(']', '');  
        error.output.payload.description= "Fail";
        error.output.payload["error"]={"errorCode":110,"errorDescription":Msg};
        return reply(error);
    }
}
