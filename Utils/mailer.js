var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
const configs = require('../Configs');
const env = require('../env');
const mailer  = (env.instance == "dev") ? configs.mailer.dev   :  configs.mailer.test;
//console.log(mailer);

var transporter = nodemailer.createTransport(smtpTransport({
    host: mailer.smtpServer,
    port: mailer.smtpPort,
    auth: {
        user: mailer.smtpUser,
        pass: mailer.smtpPass
    }
}));

module.exports ={
	transporter: transporter
}