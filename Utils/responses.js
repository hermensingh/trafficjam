module.exports = {
	systemError : {
		statusCode: 500,
		status: 'error',
		description: 'Technical error ! Please try again later.'
	},
	emailEmpty: {
		statusCode: 400,
		status: "warning",
		description: 'Enter your email address to send activation link.'
	},
	emailAlreadyExists: {
		status: 400,
		description:"Fail",
		error:{
			errorCode:101,
			errorDescription: 'Email already exists. Please try again with different email address.'
		}
	},
	usernameAlreadyExists: {
		status: 400,
		description: 'Fail',
		error:{
			errorCode:101,
			errorDescription:"Username already exists. Please try again with different Username address."
		}
	},
	emailIdNotExists:{
		status: 400,
		description: 'Fail',
		error:{
			errorCode:100,
			errorDescription:"Email not exists with this account. Please update your email fisrt."
		}
	},
	emailNotExists:{
		status: 400,
		description: 'Fail',
		error:{
			errorCode:100,
			errorDescription:"Email not exists. Please try again with different email address."
		}
	},
	invalidpassword:{
		status: 400,
		description: 'Fail',
		error:{
			errorCode:100,
			errorDescription: 'Given Password is incorrect.'
		}
	},
	notconfirmed:{
		status: 400,
		description: 'Fail',
		error:{
			errorCode:100,
			errorDescription: 'Account is not confirmed. Please check your email for activation link or resend it using your email ID'
		}
	},
	suspended:{
		status: 400,
		description: 'Fail',
		error:{
			errorCode:100,
			errorDescription: 'Account is suspended. Please contact administrator'
		}
	},
	deleted:{
		status: 400,
		description: 'Fail',
		error:{
			errorCode:100,
			errorDescription: 'Account is deleted. Please contact administrator'
		}
	},
	isConfirmed:{
		status: 400,
		description: "Your account has been activated.",
		data:{
		}
	},
	alreadyConfirmed:{
		status: 400,
		statusCode: "warning",
		description: 'Account is already confirmed, use login or forgot password to access your account.'
	},
	tokenNotExist: {
		status: 400,
		description: "Fail",
		error:{
			errorCode:101,
			errorDescription:"Given token does not exist."
		}
	},
	idNotExist:  {
		status: 400,
		description: 'Fail',
		error:{
			errorCode:100,
			errorDescription: 'Given Id does not exist.'
		}
	},	
	loginSuccessfull : {
		status: 200,
		description: 'Login successful.',
		data:{

		}
	},
	logoutSuccessfull : {
		status: 200,
		description: 'Logout successful.',
		data:{

		}
	},
	registerationSuccessfull : {
		status: 200,
		description: "Registeration successful. Please check your email."
	},
	profileUpdated:{
		status: 200,
		description: "Profile successfully updated.",
		data:{

		}
	},
	updatepassword:{
		status: 200,
		description: "Your Password updated successfully.",
		data:{

		}
	},
	resetPasswordSuccessfull:{
		status: 200,
		description: "Your Password reset request sent to your registered emailID.",
		data:{

		}
	},
	idOrTokenNotExist:{
		status: 400,
		description: "Fail",
		error:{
			errorCode:101,
			errorDescription:"Given id or token does not exist."
		}

	},
	fileWriteError:{
		status: 400,
		statusCode: "Fail",
		description: "Error in writing file"
	},
	imageUploadSuccessfull:{
		status: 200,
		statusCode: "success",
		description: "Image uploaded successfully"
	},
	jamInserted:{
		status: 200,
		description: "Jam Audio uploaded successfully",
		data:{

		}
	},
	locationSuccessfull:{
		status: 200,
		statusCode: "success",
		description: "Location updated successfully"
	},
	alreadyDefaultImage:{
		statusCode: 100,
		statusCode: "false",
		description: "This is already default image.Please choose any other image"
	},
	noMatchingResultFound:{
		statusCode: 100,
		statusCode: "true",
		description: "No matching results found"
	}
}
	
