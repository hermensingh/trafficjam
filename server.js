/*
 * @file: server.js
 * @description: This is the main startup server file to init the application
 * @date: 27/07/16
 * @author: Nitin Padgotra
 * */

//NODE_ENV=dev pm2 start server.js --name traffic_jam --log-date-format 'YYYY-MM-DD-HH:mm-Z'

// include external modules
const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const mongoose = require('mongoose');

// include internal modules
const configs = require('./Configs');
const env = require('./env');
const app = (env == "dev") ? configs.app.test   :  configs.app.dev;
const db  = (env == "dev") ? configs.database.test   :  configs.database.dev;
const server = new Hapi.Server();
var routes = require('./Routes');

server.connection({
    host: app.host,
    port: app.port,
    routes:{
        cors:true
    }
});


server.route(routes);

// init the index route
server.route({
    method: 'GET',
    path:'/',
    handler: function (request, reply) {

        return reply({
            name: app.name,
            endpoint: app.host,
            port: app.port

        });
    }
});




// Start the server

const options = {
    info: {
        'title': 'Traffic Jam API Documentation',
        'version': '1.0.0'
    },
    basePath: '/v1'
};

server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], function(err)  {
    server.start( function(err)  {
        if (err) {

            console.log("+++++++++++++++++++++++ Error starting server +++++++++++++++++++++");
            throw err;
        } else {
            console.log('+++++++++++++++++++++++ SERVER STARTED +++++++++++++++++++++++++++ \n\r Server running at:' + server.info.uri);
    }
    });
});
var mongoUrl = 'mongodb://'+db.host+':'+db.port+'/'+db.database;


console.log(mongoUrl);


//Connect to MongoDB
 mongoose.connect(mongoUrl, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected', mongoUrl);
    }
});
