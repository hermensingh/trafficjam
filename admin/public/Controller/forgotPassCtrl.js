TrafficJam.controller('forgotPassCtrl', function($scope, $http) {
	 $scope.message="";
        $scope.updatePass = function(isValid) {
        	 if (isValid) {
                var loc=window.location.href;
                loc=loc.substring(loc.indexOf('=')+1,loc.indexOf('#'));
                console.log(loc);
        	 	var req = {
					method: 'PUT',
					url: 'http://localhost:3000/v1/Users/resetPassword',
					headers: {
					'Content-Type': 'application/json'
					},
					data:{
						resetpassword_token:loc,
						newpassword:$scope.formData.password
					}
					};
        	 	$http({
                        url: 'http://localhost:3000/v1/Users/resetPassword',
                        method: "POST",
                        data:{
                            "resetpassword_token":loc,
                            "newpassword":$scope.formData.password
                        },
                        // withCredentials: true,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        }
                }).then(function(successCallback) {
        	 		console.log("success");
        	 		$scope.message = "Password Reset Successfully";
        	 	},function(errorCallback) {
        	 		console.log("error");
        	 	});
      } else {
        $scope.message = "There are still invalid fields";
      }
    };
});