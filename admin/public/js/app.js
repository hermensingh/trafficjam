/*
* Main app.js file
*/


var TrafficJam = angular.module('TrafficJam', ['ui.router']);


TrafficJam.config(function($stateProvider, $urlRouterProvider) {

    // For any unmatched url, redirect to /404
    $urlRouterProvider.otherwise("/");

    // Now set up the states
    $stateProvider
        .state('/#/', {
            url: "/#/",
            templateUrl: "index.html"
        })
});