/*
 * @file:jammer
 * @description:Jammer Routing
 * @date: 26/09/2016
 * @author:Hermenpreet/Harpreet
 * */


'use strict';
// include external modules
var Joi = require('joi');

// include internal modules
var Controller = require('../Controllers');
var Utils = require('../Utils/commonfunctions').require_login;
var UtilsFailAction = require('../Utils');
module.exports = [
    {
        method: 'POST',
        path:'/v1/Jammer/uploadJamAudio',
        config: {
            description: 'upload Jam Audio with current latitude longitude',
            notes: 'upload jam file with latitude longitude of user',
            tags: ['api'],
            pre: [{ method: Utils, assign: 'verify' }], // middleware to verify logintoken before proceeding
            validate:{
                payload : {
                    logintoken: Joi.string().required(),
                    type: Joi.string().required(),
                    buffer: Joi.string().required(),
                    duration:Joi.string().optional()
                },
                failAction: UtilsFailAction.commonFunctions.failActionFunction
            }
        },
        handler: function (request, reply) { //
            request.payload.user_id=request.pre.verify.data.id;
            Controller.jammer.jammerInsert(request.payload,function(err,result){
                if (err) {
                    reply(err);
                } else {
                    reply(result).header('X-logintoken', result.auth_token);
                }
            });
        }
    }
];


