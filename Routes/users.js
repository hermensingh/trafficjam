/*
 * @file: users.ja-Routes
 * @description: user routes define
 * @date:21/09/2016
 * @author:harpreet/hermenpeet
 * */


'use strict';
// include external modules
var Joi = require('joi');

// include internal modules
var Controller = require('../Controllers');
var Utils = require('../Utils/commonfunctions').require_login;
var UtilsFailAction = require('../Utils');

module.exports = [
    {
        method: 'POST',
        path: '/v1/Users/login', // login api
        config: {
            description: 'User Login',
            notes: 'Login to the system',
            tags: ['api'],
            validate: {
                payload: {
                    email: Joi.string().email().lowercase().required(),
                    password: Joi.string().required().min(6),
                    device_token:Joi.string().optional(),
                    gcm_id:Joi.string().optional()
                },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
            }
        },
        handler: function(request, reply) {
            Controller.User.login(request.payload, function(err, res) {
                if (err) {
                    reply(err);
                } else {
                    reply(res).header('X-logintoken', res.data.auth_token);
                }
            });
        }
    },
    {
        method:'POST',
        path:"/v1/Users/forgotPassword",
        config:{
            description:'forget Password',
            notes:'Reset a users password using emailID',
            tags:['api'],
            validate:{
                payload:{
                    email:Joi.string().email().lowercase().required()
                },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
            }
        },
        handler:function(request,reply){
          Controller.User.forgotPassword(request.payload,function(err,res){
             if(err)
                reply(err);
              else
                reply(res).header('X-logintoken', res);
          }); 
        }
    },
    {
      method:'POST',
      path:'/v1/Users/logout/{id}',          //logout api
      config:{
        description: 'User Logout',
        notes: 'Logout from the system',
        tags:['api'],
        validate:{
            params: {
            id: Joi.string().length(24).required()
            },
        failAction: UtilsFailAction.commonFunctions.failActionFunction
        }
      },
      handler:function(request,reply){
       Controller.User.logOutUser(request.params,function(err,res){
        if(err)
            reply(err)
        else
            console.log(res);
             reply(res).header('X-logintoken', res);
       });
      }
    }, 
    {
            method: 'PUT',
            path: '/v1/Users/changePassword', // change password...
            config: {
                description: 'Change Password',
                notes: 'Update a users password using old password',
                tags: ['api'],
                pre: [{ method: Utils, assign: 'verify' }], // middleware to verify logintoken before proceeding
                validate: {
                    payload: {
                        userid: Joi.string().length(24).required(),
                        logintoken: Joi.string().required(),
                        oldpassword: Joi.string().required(),
                        newpassword: Joi.string().min(6).required()
                    },
             failAction: UtilsFailAction.commonFunctions.failActionFunction
                }
            },
            handler: function(request, reply) {
                if (request.pre.verify.data) {
                    Controller.User.changepassword(request.payload, function(err, res) {
                        if (err) {
                            reply(err);
                        } else {
                            reply(res);
                        }
                    });
                } else {
                    reply({ statusCode: 200, status: "fail", description: "id or token is invalid" });
                }
            }
    },
    {
           method: 'GET',
            path: '/v1/Users/getuserdata/{logintoken}/{id}', // Get User's profile
            config: {
                description: 'Get User profile',
                notes: 'Get Users profile',
                tags: ['api'],
                pre: [{ method: Utils, assign: 'verify' }], // middleware to verify logintoken before proceeding
                validate: {
                    params: {
                        logintoken: Joi.string().required(),
                        id:Joi.string().length(24).required()
                    },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
                }
            },
            handler: function(request, reply) {
                Controller.User.getuserProfile(request.params.id, function(err, res) {
                    if (err) {
                        reply(err);
                    } else {
                        reply({ status: 200, message: "User Profile Data", data:res});
                    }
             });
    }
 }, 
 {
            method: 'PUT',
            path: '/v1/Users/updateuser/{user_id}', // Update User's profile
            config: {
            description: 'Update profile & create profile',
            notes: 'Update Users profile',
            tags: ['api'],
            pre: [{ method: Utils, assign: 'verify' }], // middleware to verify logintoken before proceeding
            validate: {
                payload: {
                    logintoken: Joi.string().required(),
                    fullname:Joi.string().required(),
                    username:Joi.string().required(),
                    dob:Joi.string().required(),
                    gender:Joi.number().required(),
                    mobile:Joi.string().optional(),
                },
                params: {
                    user_id: Joi.string().required()
                },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
            }
       },
        handler: function(request, reply) {
            Controller.User.updateprofile(request, function(err, res) {
                if (err) {
                    reply(err);
                } else {
                    reply(res.array);
                }

            });
        }
}, 
{ 
    method: 'GET',     // verify users account
    path: '/v1/Users/verifyAccount/{token}',
    config: {
        description: 'Verify User',
        notes: 'Verify the user on confirm account',
        tags: ['api'],
        validate: {
            params: {
                token: Joi.string().required()
            },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
        }
    },
    handler: function(request, reply) {
        Controller.User.confirmAccount(request.params, function(err, res) {
            if (err) {
                reply(err);
            } else {
                reply(res);
            }
        });
    }
 },
{
    method: 'POST',
    path: '/v1/Users/resetPassword', //Password updation
    config: {
                description: 'Reset new password',
                notes: 'Reset new password',
                tags: ['api'],
                validate: {
                    payload: {
                    resetpassword_token: Joi.string().required(),
                    newpassword: Joi.string().min(6).required(),//.min.max(12).required().rege/^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z]{6,}$/).options({language: {string: {regex: { base: 'must contain one digit'}}}})
            }
        }
    },
    handler: function(request, reply) {
        Controller.User.resetNewPassword(request.payload, function(err, res) {
            if (err) {
                reply(err);
            } else {
                reply(res);
            }
        });
    }
},
{
    method: 'POST',
    path: '/v1/Users/profileImage',
    config: {
        description: 'upload Image ',
        notes: 'Profile pic update',
        tags: ['api'],
        pre: [{ method: Utils, assign: 'verify' }], // middleware to verify logintoken before proceeding
        validate: {
            payload: {
                    logintoken: Joi.string().required(),
                    id:Joi.string().length(24).required(),
                    filename: Joi.string().required(),
                    type: Joi.string().required(),
                    buffer: Joi.string().required()
                },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
            }
    },
    handler: function(request, reply) {
        request.payload.user_id=request.pre.verify.data.id;
        Controller.User.uploadImg(request.payload, function(err, res) {
            if (err) {
                reply(err);
            } else {
                reply(res);
            }
        });
    }
},
{
    method: 'POST',
    path: '/v1/Users/emailUpdate', //Email updation
    config: {
        description: 'email Update and verify for account Confirmation ',
        notes: 'email update',
        tags: ['api'],
        validate: {
            payload: {
                    token: Joi.string().required(),
                    id:Joi.string().length(24).required(),
                    email: Joi.string().email().lowercase().required()
                },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
            }
    },
    handler: function(request, reply) {
        Controller.User.emailUpdate(request.payload, function(err, res) {
            if (err) {
                reply(err);
            } else {
                reply(res);
            }
        });
    }
},
{
   method:'POST',
   path:'/v1/Users/updateLocation',
   config:
   {
        description:'update location of user in traffic jam',
        notes:'update location according to given latitude longitude',
        tags:['api'],
        pre: [{ method: Utils, assign: 'verify' }], // middleware to verify logintoken before proceeding
        validate: {
            payload: {
                         logintoken: Joi.string().required(),
                         latitude:Joi.number().min(-90).max(90).required(),
                         longitude:Joi.number().min(-180).max(180).required()
                     },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
        }
   },
   handler:function(request,reply){
        request.payload.user_id=request.pre.verify.data.id;
        Controller.User.upadteLocation(request.payload, function(err, res) {
            if (err) {
                reply(err);
            } else {
                reply(res);
            }
        });
    }
}

];


