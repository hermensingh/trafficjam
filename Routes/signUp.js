/*
 * @file:Signup
 * @description:Signup Routing
 * @date: 21/09/2016
 * @author:Hermenpreet/Harpreet
 * */


'use strict';
// include external modules
var Joi = require('joi');
var UtilsFailAction = require('../Utils');

// include internal modules
var Controller = require('../Controllers');

module.exports = [
    {
        method: 'POST',
        path:'/v1/Users/SignUp',
        config: {
            description: 'Add new users',
            notes: 'Add a new user to the Traffic jam',
            tags: ['api'],
            validate:{
                payload : {
                    email_id: Joi.string().email().lowercase().when('auth_type', { is: 0, then: Joi.required(), otherwise: Joi.optional() }), //0 for email required otherwise may be null
                    password: Joi.string().min(6).when('auth_type', { is: 0, then: Joi.required(), otherwise: Joi.optional() }),
                    auth_type:Joi.number().required(),
                    role:Joi.number().required().default(2),
                    fb_id:Joi.string().when('auth_type', { is: 1, then: Joi.required(), otherwise: Joi.optional() }),
                    fullname:Joi.string().when('auth_type', { is: 0, then: Joi.optional(), otherwise: Joi.required() }),
                    dob:Joi.string(),
                    gender:Joi.number().when('auth_type', { is: 0, then: Joi.optional(), otherwise: Joi.required() }),
                    mobile:Joi.string().optional(),
                    twitter_id:Joi.string().when('auth_type', { is: 2, then: Joi.required(), otherwise: Joi.optional() }),
                    device_token:Joi.string().optional(),
                    gcm_id:Joi.string().optional()
                },
            failAction: UtilsFailAction.commonFunctions.failActionFunction
            }
        },
        handler: function (request, reply) { //
            Controller.signUp.signUp(request.payload,function(err,result){
                if (err) {
                    reply(err);
                } else {
                    reply(result).header('X-logintoken', result.auth_token);
                }
            });
        }
    }
];


