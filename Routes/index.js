/*
 * @file:index
 * @description: main route file for access othe files of route directory
 * @date: 21/09/2016
 * @author:Hermenpreet/Harpreet
 * */


'use strict';


var users =  require('./users');
var signUp =  require('./signUp');
var jammer =  require('./jammer');



module.exports = [].concat(users,signUp,jammer);
