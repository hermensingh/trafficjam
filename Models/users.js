/*
 * @file: users.js
 * @description: user collection
 * @date: 27/07/16
 * @author: Harpreet/Hermenpreet
 * */
var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var UserSchema = new Schema({
    auth_type: { type: Number, default: 0}, // 0 manual, 1 facebook, 2 twitter
    role: { type: Number, default: 2}, // 1 admin,2 user
    fullname: { type: String},
    username: { type: String},
    email: { type: String},
    gender: { type: Number },
    password: { type: String},
    imageUrl: { type: String},     
    contactNumber: { type: String},
    fb_id:{type:String},
    twitter_id:{type:String},
    dob:{type:String},
    created_at: { type: Date, default: Date.now },
    modified_at: { type: Date },
    last_login: { type: Date },
    is_suspended: { type: Boolean, default: false },
    is_deleted: { type: Boolean, default: false },
    is_verified: { type: Boolean, default: false },
    login_token: { type: String , default: 0},
    token:{ type: String , default: 0},
    reset_token:{ type: String , default: 0},
    device_token:String,
    is_confirmed: { type: Boolean ,default: false},
    is_first_login: {type:Boolean, default: true},
    newsletter: {type:Boolean, default:false},
    gcm_id:{type:String},
    location : {
      type: 
        {
            type: String, enum: "Point", default: "Point"
        },
      coordinates: 
        {
            type: [Number], default: [0, 0]
        }
    }
});

var user = Mongoose.model('user', UserSchema);
module.exports = {
    User: user
};