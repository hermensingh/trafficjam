/*
 * @file:index
 * @description:Main file to access all models
 * @date: 21/09/2016
 * @author: Hermenpreet/Harpreet
 * */

module.exports={
	users: require('./users'),
	jamfiles:require('./jammer')
};