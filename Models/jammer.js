/*
 * @file: jammer.js
 * @description: jammer collection
 * @date: 27/07/16
 * @author: Harpreet/Hermenpreet
 * */


 var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var JammerSchema = new Schema({
	user_id:{type:Schema.Types.ObjectId,ref:"user"},
    audiourl:String,
    filetype:String,
    duration:String,
    // image: { data: Buffer, contentType: String },
    created_at: { type: Date, default: Date.now },
    modified_at: { type: Date },
    // jamlocation:{
    //     'type': {type: String, enum: "Point", default: "Point"},
    //     coordinates: {type: [Number], default: [0, 0]}
    // }
});

var jamfiles = Mongoose.model('jamfiles', JammerSchema);
module.exports = {
    Jamfiles: jamfiles
};