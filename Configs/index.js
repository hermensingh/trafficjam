/*
 * @file: index.js
 * @description: main module to incluse all the configs
 * @date: 27/07/16
 * @author: Nitin Padgotra
 * */

'use strict';

module.exports =  {

    database: require('./dbConstants'),
    app: require("./appConstants"),
     mailer: require("./mailConstants"),
    config: require("./configConstants")
};





