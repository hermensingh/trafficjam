/*
 * @file: appConstants.js
 * @description: includes all the app settings
 * @date: 27/07/16
 * @author: Nitin Padgotra
 * */
var url="http://localhost:3000";
module.exports = {

    dev: {
        name: "Traffic Jam",
        host: "127.0.0.1",
        port: "3000",
        debug: true
    },
    test: {
        name: "Cork Screw",
        host: "127.0.0.1",
        port: "3001",
        debug: true
    },
    live: {
        name: "Cork Screw",
        host: "127.0.0.1",
        port: "3002",
        debug: true
    },
    url:url

};


