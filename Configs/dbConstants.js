/*
 * @file: dbConstants.js
 * @description: includes all the db settings
 * @date: 27/07/16
 * @author: Nitin Padgotra
 * */

module.exports = {

    dev: {
        host: "localhost",
        username: "",
        password: "",
        port: "27017",
        database: "traffic_jam"
    },
    test: {
        host: "",
        username: "",
        password: "",
        port: "",
        database: ""
    },
    live: {
        host: "",
        username: "",
        password: "",
        port: "",
        database: ""
    }


};