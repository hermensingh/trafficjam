module.exports = {
	data:{
		jwtAlgo: 'HS512',
		jwtkey: 'lmvignivasolutions',
		bcryptSaltRound: '10',
		noReplyEmail: 'noreply@lmv.com',
		adminEmail: 'harpreet.kaur@ignivasolutions.com'
	}

}