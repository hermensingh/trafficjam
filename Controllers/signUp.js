/*
 * @file:signUp/Controller
 * @description: SignUP controller file
 * @date: 21/09/2016
 * @author:Hermenpreet/Harpreet
 * */


var service=require("../Services");
module.exports = {
	signUp: function (params,callback) {
		service.signUp.registerUser(params,callback);
		//validations
		
	}
};