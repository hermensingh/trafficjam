/*
 * @file:index
 * @description: main controller file for access other files of controller directory
 * @date: 21/09/2016
 * @author:Hermenpreet/Harpreet
 * */

'use strict';

module.exports =  {
    User: require('./users'),
    signUp: require('./signUp'),
    jammer: require('./jammer')
};


