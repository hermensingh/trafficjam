/*
 * @file:usres/Controller
 * @description: users controller file
 * @date: 21/09/2016
 * @author:Hermenpreet/Harpreet
 * */
var Services=require("../Services");
module.exports = {
	login: function(request,callback){
			Services.user.login(request,function(err, res){
				if(err)
	                callback(err)
	            else 
	                callback(null,res)
			});
	},
	forgotPassword:function(request,callback){
			Services.user.forgotPassword(request,function(err,res){
	          if(err)
	          	callback(err)
	          else
	          	callback(null,res);
			});
	},
	logOutUser:function(request,callback){
			Services.user.logOutUser(request,function(err,res){
	         if(err)
	          	callback(err)
	          else
	          	callback(null,res);
			});
	},
	changepassword: function(request, callback){
			Services.user.changepassword(request, function(err, res){
				if(err) 
	                callback(err);
	             else 
	               callback(null,res);;				
			});
	},
	getuserProfile: function(request, callback){
			Services.user.getuserProfile(request, function(err, res){
				if(err)
					callback(err)
				else
					callback(null, res)
			});
	},
	updateprofile: function(request, callback){
			Services.user.updateprofile(request, function(err, res){
				if(err) 
	                callback(err)
	             else 
	                callback(null, { count: res.length, array: res})
			});
	},
	confirmAccount: function(request, callback){
			Services.user.verifyActivation(request, function(err, res){
				if(err)
	                callback(err)
	            else 
	                callback(null, { count: res.length, array: res})				
			});
	},
	resetNewPassword: function(request, callback){
		console.log(request);
			Services.user.resetNewPassword(request, function(err, res){
				if(err) 
	                callback(err)
	            else 
	                callback(null, res);        				
			});
	},
	uploadImg:function(request,callback){
		Services.user.uploadImg(request,function(err,res){
			if(err) 
	                callback(err)
              else 
	                callback(null, res);
				
		})
	},
	emailUpdate:function(request,callback){
		Services.user.emailUpdateAndVerify(request,function(err,res){
			if(err) {
	                callback(err)
	            } else {
	                callback(null, res);
	            }				
		})
	},
	upadteLocation:function(request,callback){
		Services.user.upadteLocation(request,function(err,res){
           if(err)
           	 callback(err)
           else
           	 callback(null,res);
		});
	}

};